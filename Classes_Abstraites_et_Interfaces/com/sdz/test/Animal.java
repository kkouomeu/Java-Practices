package com.sdz.test;


abstract class Animal {
	
	//Variables d'instance
	protected int poids;
	protected String couleur;

	protected void manger() {
		System.out.println("je mange de la viande");
	}

	protected void boire() {
		System.out.println("Je bois de l'eau");
	}

	abstract void deplacement();

	abstract void crier();	

}
