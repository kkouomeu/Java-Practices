package com.sdz.test;

public class Chien extends Canin implements Rintintin {

	 public Chien() {
		
	}
	 
	 public Chien(int poids,String couleur) {
		 this.couleur = couleur;
		 this.poids = poids;
	 }
	 
	
	public void faireCalin() {
		System.out.println("je fais des calins");
	}
	
	@Override
	public void faireLeBeau() {
		System.out.println("je fais le beau");
	}
	
	@Override
	public void faireLeChouille() {
		System.out.println("je fais des grosses lechouilles");
	}
	
	@Override
	void crier() {
		
		System.out.println("j'aboie !");
	}

}
