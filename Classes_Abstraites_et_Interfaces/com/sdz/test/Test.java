package com.sdz.test;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Chien l = new Chien(25,"blanc");
		l.boire();
		l.manger();
		l.deplacement();
		l.crier();
		
		System.out.println(l.toString());	
		
		System.out.println("------------------------------------------");
		//les methodes de l'interface
		l.faireCalin();
		l.faireLeBeau();
		l.faireLeChouille();
		
		System.out.println("------------------------------------------");
		//le polymorphisme
		Rintintin r = new Chien();
		
		r.faireCalin();
		r.faireLeBeau();
		r.faireLeChouille();
		
	}

}
