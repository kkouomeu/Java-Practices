package src;

public enum Langage {
	//Objets construits
	JAVA ("Langage JAVA","eclipse"),
	C ("Langage C","code block"),
	CPlus(" Langage C++","visual studio"),
	PHP("Langage PHP" , "ps pad");
	
	
	private String name = "";
	private String editor = "";
	
	//Constructeur
	Langage(String name,String editor){
		this.name = name ;
		this.editor = editor;
	}
	
	public void getEditor() {
		System.out.println("L'editeur est : " + editor);
	}
	
	public String toString() {
		return name;
	}
	
}
