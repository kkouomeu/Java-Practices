package tutos;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class CapitalesGames {

	public static void main(String[] args) {

		final int NUMBER_OF_QUESTIONS = 6;
		int index;
		String pays, capitale;
		int cmpt = 0;

		ArrayList<Integer> IndexAllReadyTaken = new ArrayList<>();

		String[][] datas = {

				{"Senegal", "Mali"},
				{"France", "Paris"},
				{"Nigeria", "Lagos"},
				{"Gabon", "Libreville"},
				{"Allemagne", "Berlin"},
				{"Italie", "Rome"},
				{"Perou", "Lima"},
				{"Liberia", "Monrovia"},
				{"Monaco", "Monaco"},
		};



		Scanner keyboard = new Scanner(System.in);

		for (int i = 0; i < NUMBER_OF_QUESTIONS ; i++) {
			//choisir un pays de maniere aleatoire

			do {

				Random random = new Random();
				index = random.nextInt(datas.length);

			} while (IndexAllReadyTaken.contains(index));

			IndexAllReadyTaken.add(index);

			pays = datas[index][0];
			capitale = datas[index][1];

			System.out.println("Quelle est la capitale de ce pays ? : "+ pays);		


			//Demander au user d'entrer la capitale du pays
			String answer = keyboard.nextLine();
			answer = answer.substring(0, 1).toUpperCase() + answer.substring(1).toLowerCase();

			//comparer les reponses

			if (answer.equals(capitale)) {
				//afficher le verdict	
				System.out.println("---------------------------------------------------- \n Bonne r�ponse ! ");
				cmpt++;
			} else {
				System.out.println("------------------------------- \n Mauvaise r�ponse ! il fallait repondre "+ capitale);
			}

		}
		
		keyboard.close();

		//afficher le resultat final
		System.out.println("\n\n Le jeu est termin� ! \n Vous avez trouver : " + cmpt + "/" + NUMBER_OF_QUESTIONS + ".");

	}


}
