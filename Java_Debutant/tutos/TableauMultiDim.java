package tutos;

public class TableauMultiDim {
	public static void main(String[] args) {
		int[][] scores = {
				{1,2,3,6,5,8},
				{14,5,33,4,0,1},
				{104,15,3,4,05,51}
		};
		
		for (int i = 0; i < scores.length; i++) {
			for (int j = 0; j < scores[i].length; j++) {
				System.out.print(scores[i][j] + " ");
			}
			System.out.println();
		}
	}
}
