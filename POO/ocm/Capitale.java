package ocm;

public class Capitale extends Ville {

	private String monument;
	
	public String getMonument() {
		return monument;
	}
	
	public void setMonument(String monument) {
		this.monument = monument;
	}
	
	//Constructeur par defaut
	public Capitale() {
		//appele le constructeur de la classe mere
		super();
		monument = "aucun";
	}
	
	//Constructeur d'initialisation
	public Capitale(String nom, String pays, int habitants, String monument) throws NombreHabitantsException {
		//appele la methode de la classe mere
		super(nom,pays,habitants);
		this.setMonument(monument);
	}
	
	public void description() {
		super.description();
		System.out.print(this.getMonument() + " en est un monument \n\n" );
	}
	
}
