package ocm;

public class Ville {

	protected String nomVille, nomPays;
	protected int nbreHabitants;
	protected char categorie;
	//variable de classe ( toujours declar�e static )
	protected static int nbreInstanceBis = 0;


	//getters
	public char getCategorie() {
		return categorie;
	}

	public String getNomVille() {
		return nomVille;
	}

	public String getNomPays() {
		return nomPays;
	}

	public int getNbreHabitants() {
		return nbreHabitants;
	}

	public static int getNbreInstance() {
		return nbreInstanceBis;
	}

	//setters
	//Definir la categorie

	public void setCategorie() {
		int borneSup[] = {0,1000,10000,100000,1000000,10000000,100000000};
		char categorie[] = {'?','A','B','C','D','E','F','G'};

		int i= 0;
		while (i < borneSup.length && this.nbreHabitants > borneSup[i]) {
			i++;
		}

		this.categorie = categorie[i];
	}

	public void setNbreHabitants(int nbreHabitants) {
		this.nbreHabitants = nbreHabitants;
	}

	public void setNomPays(String nomPays) {
		this.nomPays = nomPays;
	}

	public void setNomVille(String nomVille) {
		this.nomVille = nomVille;
	}




	//constructeur par d�faut
	public Ville() {
		System.out.println("Creation d'une ville");
		nomVille = "inconnu";
		nomPays = "inconnu";
		nbreHabitants = 0;
		this.setCategorie();
		nbreInstanceBis++;
	}

	//constructeur avec parametres
	public Ville(String pNomVille, String pNomPays, int pNomHabitants) throws NombreHabitantsException {

		if (pNomHabitants < 0) {
			throw new NombreHabitantsException();
		}else {
			nomVille = pNomVille;
			nomPays = pNomPays;
			nbreHabitants = pNomHabitants;
			this.setCategorie();
			
			nbreInstanceBis++;
		}

	}

	//Description d'un ville
	public void description() {
		System.out.println(
				"ville : " + this.getNomVille() + "\n" +
						"Pays : " + this.getNomPays() + 
						"\n" + "Nombre d'habitants : " + this.getNbreHabitants() +
						"\n" + "Donc vous etes de cat�gorie : << " + this.getCategorie() + " >> "
				);
	}

	//Comparer en fct du nbre d'habitants
	public void compare(Ville pVille) {
		if (this.getNbreHabitants() > pVille.getNbreHabitants()) {
			System.out.println(pVille.getNomVille() + " a plus d'habitants que " + this.getNomVille());
		}else if (this.getNbreHabitants() < pVille.getNbreHabitants()) {
			System.out.println(pVille.getNomVille() + " a moins d'habitants que " + this.getNomVille());
		}else {
			System.out.println(pVille.getNomVille() + " a le meme nombre d'habitants que " + this.getNomVille());
		}
	}

}
