package com.sdz.jeuDeGuerre;

import com.sdz.jdg.comportement.*;


public class Chirurgien extends Personnage{
	
	public Chirurgien() {
		this.deplacement = new Courir();
		this.soin = new Operation();
	}
	
	public Chirurgien(EspritCombatif esp, Soin sn, Deplacement dep) {
		super(esp,sn,dep);
	}
	
}
