package com.sdz.jeuDeGuerre;

import com.sdz.jdg.comportement.*;

public class Guerrier extends Personnage{
	
	public Guerrier() {
		// TODO Auto-generated constructor stub
		this.espritCombatif = new CombatPistolet();
	}
	
	public Guerrier(EspritCombatif esprit,Soin soin, Deplacement dep ) {
		super(esprit,soin,dep);
	}
}
