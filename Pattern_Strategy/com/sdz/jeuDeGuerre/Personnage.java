package com.sdz.jeuDeGuerre;

import com.sdz.jdg.comportement.*;

public abstract class Personnage {
	
	//Instance de comportement du perso
	protected EspritCombatif espritCombatif = new Pacifiste();
	protected Soin soin = new AucunSoin();
	protected Deplacement deplacement = new Marcher();
	
	//constructeur par d�faut
	public Personnage() {}
	
	//constructeur avec parametres
	public Personnage(EspritCombatif espritCombatif,Soin soin,Deplacement deplacement){
		this.espritCombatif = espritCombatif;
		this.soin = soin;
		this.deplacement = deplacement;
	}
	
	//methode de deplacement d'un perso
	public void seDeplacer() {
		deplacement.deplacer();
	}
	
	//methode de combat d'un perso
	public void combattre() {
		espritCombatif.combat();
	}
	
	//methode de soin d'un perso
	public void soigner() {
		soin.soigner();
	}
	
	public void setEspritCombatif(EspritCombatif espritCombatif) {
		this.espritCombatif = espritCombatif;
	}
	
	public void setDeplacement(Deplacement deplacement) {
		this.deplacement = deplacement;
	}
	
	public void setSoin(Soin soin) {
		this.soin = soin;
	}
	
}
